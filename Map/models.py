from django.db import models
from django.contrib.gis.db import models as gismodels
import csv
from django.contrib.gis.geos import Point

from webmap.models import WeatherStation


class WeatherStation(gismodels.Model):

    wmoid = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256)

    geom = gismodels.PointField()

    objects = gismodels.GeoManager()

    def __unicode__(self):
        return self.name


csv_file = 'london.txt'

reader = csv.DictReader(open(csv_file, 'rb'), delimiter="\t")
for line in reader:
    lng = line.pop('Longitude')
    lat = line.pop('Latitude')
    wmoid = int(line.pop('Crime ID'))
    name = line.pop('Month').title()

    WeatherStation(wmoid=wmoid, name=name, geom=Point(lng, lat)).save()
