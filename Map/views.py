from django.views.generic.base import TemplateView


class MarkersMapView(TemplateView):
    """Map view."""

    template_name = "map.html"