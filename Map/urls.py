from django.urls import path

from .views import MarkersMapView

app_name = "Map"

urlpatterns = [
    path("Map/", MarkersMapView.as_view()),
]